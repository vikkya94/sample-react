import React, { Component } from 'react';
import api from '../utils/api'

class Generic extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectValue: '10g1e9'
        }
       
        this.handleChange = this.handleChange.bind(this);
        this.myapi = this.myapi.bind(this);
    }
    handleChange(e) {
        this.setState({ selectValue: e.target.value });
    }
    componentDidMount() {

        this.myapi();
      }
    myapi(){
        api.myapi(this.state.selectValue)
        .then(function(data){
            this.setState(function(){
                console.log(this.state.selectValue)
                //i want to console log the selectValue state here
                console.log(data)
                return{ data: data}
            })
        }.bind(this))
    }
    render() {
        // console.log(this.state.selectValue)
        return (
            <div>
               
                <div className="form-group">
                <label htmlFor="exampleSelect1">Select a day</label>
      <select className="form-control" id="exampleSelect1" defaultValue={this.state.selectValue} onChange={this.handleChange}>
      <option value="mk8c9">mk8c9</option>
      <option value="1f51dl">1f51dl</option>
      <option value="19s4xl">19s4xl</option>
      
      </select>
    </div>
            </div>
        )
    }
}

export default Generic;