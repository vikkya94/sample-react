var axios = require('axios');

module.exports = {
    myapi: function(id){
        var URI = window.encodeURI('http://api.myjson.com/bins/'+id);
        return axios.get(URI)
        .then(function (res){
            return res.data;
        })
    }
}