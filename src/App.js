import React, { Component } from 'react';

import Generic from './components/Generic';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Generic />
      </div>
    );
  }
}

export default App;
